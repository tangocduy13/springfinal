package fa.training.JWD.Practice.T01.service;

import fa.training.JWD.Practice.T01.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();
}
