package fa.training.JWD.Practice.T01.dto;

import fa.training.JWD.Practice.T01.models.Cert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CategoryDto {

    private Integer id;

    private String name;

    private String description;

    private Collection<Cert> list;
}
