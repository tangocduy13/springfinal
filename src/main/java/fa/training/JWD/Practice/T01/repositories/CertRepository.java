package fa.training.JWD.Practice.T01.repositories;

import fa.training.JWD.Practice.T01.models.Category;
import fa.training.JWD.Practice.T01.models.Cert;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CertRepository extends JpaRepository<Cert, String> {

    Optional<Cert> findById(String id);

    List<Cert> findByCategory(Category category);
}
