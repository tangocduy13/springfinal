package fa.training.JWD.Practice.T01.service.impl;

import fa.training.JWD.Practice.T01.models.Category;
import fa.training.JWD.Practice.T01.models.Cert;
import fa.training.JWD.Practice.T01.repositories.CertRepository;
import fa.training.JWD.Practice.T01.service.CertService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CertServiceImpl implements CertService {

    private final CertRepository certRepository;

    public CertServiceImpl(CertRepository certRepository) {
        this.certRepository = certRepository;
    }

    @Override
    public Cert save(Cert cert) {
        return certRepository.save(cert);
    }

    @Override
    public Page<Cert> findAllAndPaginated(int page) {
        Pageable pageable = PageRequest.of(page - 1, 5);
        return certRepository.findAll(pageable);
    }

    @Override
    public void delete(String id) {
        certRepository.deleteById(id);
    }

    @Override
    public Cert findById(String id) {
        return certRepository.findById(id).orElseThrow(() -> new RuntimeException("Certificate not found"));
    }

    @Override
    public List<Cert> findByCategory(Category category) {
        return certRepository.findByCategory(category);
    }

    @Override
    public Map<String, Integer> classifyByCategory() {

        List<Cert> certList = certRepository.findAll();

        Map<String, Integer> classifyMap = new HashMap<>();
        for(int i=0; i < certList.size(); i++) {
            Category tmp = certList.get(i).getCategory();
            classifyMap.put(tmp.getName(), findByCategory(tmp).size());
        }

        return classifyMap;
    }
}
