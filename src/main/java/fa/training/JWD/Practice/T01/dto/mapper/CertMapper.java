package fa.training.JWD.Practice.T01.dto.mapper;

import fa.training.JWD.Practice.T01.dto.CertDto;
import fa.training.JWD.Practice.T01.models.Cert;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CertMapper {

    Cert convertToEntity(CertDto certDto);

    CertDto convertToDto(Cert cert);
}
