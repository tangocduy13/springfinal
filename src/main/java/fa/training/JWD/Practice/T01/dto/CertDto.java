package fa.training.JWD.Practice.T01.dto;

import fa.training.JWD.Practice.T01.models.Category;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CertDto {

    @NotNull(message = "Id must be not empty!")
    @Pattern(regexp = "[a-zA-Z0-9-]+", message = "Id wrong format!")
    private String id;

    @NotNull(message = "Certification name must be not empty!")
    private String certName;

    @NotNull(message = "Cost must be not empty!")
    private Double cost;

    Category category;
}
