package fa.training.JWD.Practice.T01.dto.mapper;

import fa.training.JWD.Practice.T01.dto.CategoryDto;
import fa.training.JWD.Practice.T01.models.Category;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CategoryMapper {

    Category convertToEntity(CategoryDto categoryDto);

    CategoryDto convertToDto(Category category);
}
