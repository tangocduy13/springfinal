package fa.training.JWD.Practice.T01.controller;

import fa.training.JWD.Practice.T01.dto.CertDto;
import fa.training.JWD.Practice.T01.dto.mapper.CertMapper;
import fa.training.JWD.Practice.T01.models.Category;
import fa.training.JWD.Practice.T01.models.Cert;
import fa.training.JWD.Practice.T01.service.CategoryService;
import fa.training.JWD.Practice.T01.service.CertService;
import jakarta.validation.Valid;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/cert")
public class CertController {

    private final CertService certService;
    private final CategoryService categoryService;
    private final CertMapper certMapper;

    public CertController(CertService certService, CategoryService categoryService, CertMapper certMapper) {
        this.certService = certService;
        this.categoryService = categoryService;
        this.certMapper = certMapper;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @GetMapping("/management")
    public String certManagement(Model model,
                                 @RequestParam(value = "id", defaultValue = "")  String id,
                                 @RequestParam(value = "page", defaultValue = "1") int page) {

        CertDto certDto = new CertDto();

        if (!Objects.isNull(id)) {
            certDto = certMapper.convertToDto(certService.findById(id));
        }

        List<Category> categoryList = categoryService.getAllCategories();
        Page<Cert> certPage = certService.findAllAndPaginated(page);
        List<Cert> certList = certPage.getContent();

        Map<String, Integer> classifyMap = certService.classifyByCategory();

        model.addAttribute("map", classifyMap);
        model.addAttribute("certDto", certDto);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("certList", certList);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", certPage.getTotalPages());

        return "add_cert";
    }

    @PostMapping("/save")
    public String saveCert(@Valid @ModelAttribute("certDto") CertDto certDto,
                           BindingResult bindingResult,
                           Model model) {
        if (bindingResult.hasErrors()) {
            Page<Cert> certPage = certService.findAllAndPaginated(1);
            List<Cert> certList = certPage.getContent();
            Map<String, Integer> classifyMap = certService.classifyByCategory();
            List<Category> categoryList = categoryService.getAllCategories();

            model.addAttribute("map", classifyMap);
            model.addAttribute("certDto", certDto);
            model.addAttribute("categoryList", categoryList);
            model.addAttribute("certList", certList);
            model.addAttribute("currentPage", 1);
            model.addAttribute("totalPages", certPage.getTotalPages());

            return "add_cert";
        }

        Cert cert = certMapper.convertToEntity(certDto);
        certService.save(cert);

        return "redirect:/cert/management";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam(value = "id") String id) {
        certService.delete(id);

        return "redirect:/cert/management";
    }
}
