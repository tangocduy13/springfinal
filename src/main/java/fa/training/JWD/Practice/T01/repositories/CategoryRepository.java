package fa.training.JWD.Practice.T01.repositories;

import fa.training.JWD.Practice.T01.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
