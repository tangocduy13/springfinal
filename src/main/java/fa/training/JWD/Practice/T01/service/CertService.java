package fa.training.JWD.Practice.T01.service;

import fa.training.JWD.Practice.T01.models.Category;
import fa.training.JWD.Practice.T01.models.Cert;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface CertService {

    Cert save(Cert cert);

    Page<Cert> findAllAndPaginated(int page);

    void delete(String id);

    Cert findById(String id);

    List<Cert> findByCategory(Category category);

    Map<String, Integer> classifyByCategory();
}
