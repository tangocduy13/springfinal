package fa.training.JWD.Practice.T01.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Cert {

    @Id
    @Column(name = "id", columnDefinition = "VARCHAR(12)")
    @NotNull(message = "Id must be not empty!")
    @Pattern(regexp = "[a-zA-Z0-9-]+", message = "Id wrong format!")
    private String id;

    @Column(name = "cert_name")
    @NotNull(message = "Certification name must be not empty!")
    private String certName;

    @Column(name = "cost", columnDefinition = "DECIMAL(5,1)")
    @NotNull(message = "Cost must be not empty!")
    private Double cost;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    Category category;
}
