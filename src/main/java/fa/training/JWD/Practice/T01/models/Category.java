package fa.training.JWD.Practice.T01.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Collection;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "descriptions", columnDefinition = "VARCHAR(1000)")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
    private Collection<Cert> list;
}
